package com.testmodulescreen.network

import com.testmodulescreen.model.post.PostData
import com.testmodulescreen.model.user.UserData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface TestModuleScreenApi {



    // to get users
    @GET("users")
    fun apiGetUsers(): Call<List<UserData>>


    // to get users
    @GET("posts")
    fun apiGetUserPost(@Query ("userId") code:Int ): Call<List<PostData>>


    // to get user's todo
    @GET("todos")
    fun apiGetUserTodo(@Query ("userId") code:Int ): Call<List<PostData>>


    // to get user's albums
    @GET("albums")
    fun apiGetUserAlbum(@Query ("userId") code: String): Call<List<PostData>>


    // to get user's albums images
    @GET("photos")
    fun apiGetUserAlbumImages(@Query ("albumId") code: String): Call<List<PostData>>


/*

    // to check register or not
    @FormUrlEncoded
    @POST("user/register")
    fun apiRegister(@FieldMap map: HashMap<String, String>): Call<User>

    // final stepUp API
    @FormUrlEncoded
    @POST("user/profileSetUp")
    fun apiSetUpProfile(@Header("authorization") autorization: String,
                        @FieldMap map: HashMap<String, String>): Call<User>

    // for update user profile
    @FormUrlEncoded
    @POST("user/updateProfile")
    fun apiEditProfile(@Header("authorization") autorization: String,
                       @FieldMap map: HashMap<String, String>): Call<User>

    // for uploading image
    @Multipart
    @POST("admin/uploadImagesOrVideo")
    fun apiUploadImage(@PartMap map:HashMap<String, RequestBody>): Call<UploadImage>

    // verify otp
    @FormUrlEncoded
    @POST("user/verifyMobileNo")
    fun apiVerifyOtp(@Header("authorization") autorization: String,
                     @Field("verificationCode") code:String ): Call<User>

    // create trip
    @POST("user/addTrips")
    fun apiCreateTrip(@Header("authorization") autorization: String,
                      @Body data: CreateTripBackend): Call<Response>


    // to get categories
    @GET("user/getCategories")
    fun apiGetCategory():Call<ResponseCat>




    // to get hotel detail
    @GET("user/hotelDetails")
    fun apiGetHotelDetail(@Header("authorization") autorization: String,
                          @QueryMap map: HashMap<String, String>):Call<HotelDetail>



    // to get TripDetail
    @GET("user/tripDetails")
    fun apiTripDetail(@QueryMap map: HashMap<String, String>):Call<TripDetail>




    // to get TripDetail
    @GET("user/tripTravellerDetails")
    fun apiTripTravellerDetails(@QueryMap tripId: HashMap<String, String>):Call<TravellerDetails>





    // to get TripDetail
    @GET("user/trendingLocations")
    fun apiTrendingLocations():Call<PopularLocations>



    // to get categories
    @FormUrlEncoded
    @PUT("user/saveTrips")
    fun apiSavedTrip(@Header("authorization") autorization: String,
                     @Field("tripId") tripId:String ,
                     @Field("isSave") isSave:String ):Call<LikePojo>




    // for invite to trip
    @FormUrlEncoded
    @PUT("user/inviteToTrip")
    fun apiInviteToTrip(@Header("authorization") autorization: String,
                        @FieldMap map: HashMap<String,String>  ):Call<Follow>



    // to get categories
    @FormUrlEncoded
    @PUT("user/likeUnlikeTrips")
    fun apiLikeUnlikeTrip(@Header("authorization") autorization: String,
                          @Field("tripId") tripId:String ,
                          @Field("isLike") isSave:String ):Call<LikePojo>




    // to post comments
    @FormUrlEncoded
    @PUT("user/postComments")
    fun apiPostComment(@Header("authorization") autorization: String,
                       @Field("tripId") tripId:String ,
                       @Field("comment") comment:String ):Call<PostComment>





    // to follow or unfollow people
    @FormUrlEncoded
    @PUT("user/followUnfollowUsers")
    fun apiFollow(@Header("authorization") autorization: String,
                  @Field("userId") userId:String ,
                  @Field("isLike") isLike:Boolean ):Call<Follow>






    // to post comments
    @FormUrlEncoded
    @PUT("user/updateInstaToken")
    fun apiUpdateToken(@Header("authorization") autorization: String,
                       @FieldMap map:HashMap<String,String>):Call<User>




    // to post comments
    @FormUrlEncoded
    @PUT("user/rateUsers")
    fun apiReview(@Header("authorization") autorization: String,
                  @FieldMap map: HashMap<String,String> ):Call<AddReview>




    // to update friend list
    @FormUrlEncoded
    @PUT("user/updateFacebookFriends")
    fun apiUpdateFriendList(@Header("authorization") autorization: String,
                  @Field("friends") map:String):Call<Follow>



    //get invite trip listing
    @GET("user/listInvitedTrips")
    fun apiGetInvitedTripList(@Header("authorization") autorization: String,
                              @Query("pageNo") pageNo: String):Call<InvitedData>




    // to remove invite
    @FormUrlEncoded
    @PUT("user/acceptRejectInvitation")
    fun apiRemoveInvite(@Header("authorization") autorization: String,
                  @Field("tripId") tripId:String,
                        @Field("isAccepted") isAccepted:String):Call<Follow>






    // to add card
    @FormUrlEncoded
    @PUT("user/addCard")
    fun apiAddCard(@Header("authorization") autorization: String,
                  @FieldMap map:HashMap<String,String>):Call<Follow>




    // to get saved trips
    @GET("user/mySavedTrips")
    fun apiGetSavedTrips(@Header("authorization") autorization: String,
                         @Query("pageNo") tripId:String):Call<TripListing>



    // to get saved cards
    @GET("user/viewAllCard")
    fun apiGetSavedCards(@Header("authorization") autorization: String):Call<CardListing>


    // to get saved trips
    @GET("user/instagramImages")
    fun apiGetInstaImages(@Query("userId") userId:String,
                          @Query("pageNo") pageNo:String):Call<InstaImages>




    // to get saved trips
    @GET("user/searchUsers")
    fun apiSearchUser(@Header("authorization") autorization: String,
                      @Query("search") search:String,
                      @Query("tripId") tripId:String):Call<SearchUser>



    // to get saved trips
    @GET("user/getOthersProfile")
    fun apiGetOtherProfile(@QueryMap userId:HashMap<String,String>):Call<OtherProfileResponse>



    // to get saved trips
    @GET("user/pastTripsforProfile")
    fun apiGetPastTrips(@QueryMap userId:HashMap<String,String>):Call<PastTripResponse>


    // get my trips
    @GET("user/getMyTrips")
    fun apiGetMyTrips(@Header("authorization") autorization: String):Call<MyTripsResponse>



    // get my profile
    @GET("user/myProfileData")
    fun apiGetMyProfile(@Header("authorization") autorization: String):Call<MyProfileResponse>


    // to get categories
    @GET("user/topPicks")
    fun apiGetTopPicks(@QueryMap map: HashMap<String, String>):Call<TopPicks>


    // to get categories
    @GET("user/searchTripBycategory")
    fun apiGetTipList(@QueryMap map:HashMap<String,String>):Call<TripListing>



    // to get all comments
    @GET("user/getComments")
    fun apiGetAllComments(@Header("authorization") autorization: String,
                          @QueryMap map:HashMap<String,String>):Call<CommentListingResponse>


    // to get explore trips
    @GET("user/trendingScreen")
    fun apiGetExplore(@QueryMap map: HashMap<String, String>):Call<ExploreResponse>


    // to get categories
    @GET(AppConstants.FOURSQUARE_API_EXPLORE)
    fun apiExplore(@Query("client_id")id:String,
                   @Query("client_secret")key:String,
                   @Query("v")version:String,
                   @Query("limit")limit:String,
                   @Query("section")section:String,
                   @Query("ll")latLng:String ):Call<MainPojo>

    // for logout
    @POST("user/userLogOut")
    fun apiLogout(@Header("authorization") autorization: String):Call<User>

*/

}