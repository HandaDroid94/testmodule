package com.testmodulescreen.ui.home.firstscreen.album

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.testmodulescreen.R
import com.testmodulescreen.model.post.PostData
import com.testmodulescreen.ui.customviews.LoadingDialog
import com.testmodulescreen.utils.isNetworkActive
import com.testmodulescreen.utils.showSnack
import com.testmodulescreen.utils.showSomeWWerror
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_album_images.*
import okhttp3.ResponseBody

/**
 * Created by prashant-cbl on 19/3/18.
 */
class UserAlbumFragment : Fragment(), UserAlbumContract.View {

    private val presenter= UserAlbumPresenter()
    private var loadingBox: LoadingDialog?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_album_images,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setData(view)
        init(view)
        hitApi(view)
    }

    private fun init(view: View) {
        presenter.attachView(this)
        loadingBox= LoadingDialog(view.context)


    }

    private fun hitApi(view: View) {

        if(view.context.isNetworkActive()){
            presenter.apiGetUserAlbums(arguments?.getString("albumId","")!!)
        }
        else{
            rvUsers?.showSnack(R.string.network_error)
        }
    }

    /*  private fun setData(view: View) {

          tvTitle.text=arguments?.getString("title","")
          tvDes.text=arguments?.getString("description","")


      }*/

    override fun apiSuccess(body: List<PostData>?) {

        if(body!!.isNotEmpty()) {
            viewPager.adapter = AlbumPagerAdapter(context!!, body as ArrayList<PostData>)
            viewPager.setPadding(40, 0, 40, 0)
            viewPager.clipToPadding = false
            viewPager.pageMargin = 20
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }


    override fun showLoader(isLoading: Boolean) {
        loadingBox?.setLoading(isLoading)
    }

    override fun apiFaliure() {
        rvAlbums?.showSomeWWerror()

    }
    override fun apiError(errorBody: ResponseBody?) {
        //this.displayApiError(errorBody, view)
    }

}