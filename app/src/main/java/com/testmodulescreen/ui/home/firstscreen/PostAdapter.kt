package com.testmodulescreen.ui.home.firstscreen

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.testmodulescreen.R
import com.testmodulescreen.base.BaseActivity
import com.testmodulescreen.model.post.PostData
import com.testmodulescreen.utils.AppConstants
import kotlinx.android.synthetic.main.item_todo.view.*

/**
 * Created by prashant-cbl on 19/3/18.
 */
class PostAdapter(val context: Context?, val itemList: List<PostData>?, val type: Int):
        RecyclerView.Adapter<PostAdapter.ViewHolder>()  {
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder?.bind(itemList!![position])
    }

    override fun getItemCount(): Int = itemList?.size!!


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_todo,parent,false))

    inner class ViewHolder (itemView: View?) : RecyclerView.ViewHolder(itemView){

        init {
            itemView?.setOnClickListener{
                if(type==2){

                    if(!itemList!![adapterPosition].completed){
                        itemList[adapterPosition].completed=true
                        Toast.makeText(context,itemView.context.
                                getString(R.string.msg_completed),Toast.LENGTH_SHORT).show()


                    }else{
                        Toast.makeText(context,itemView.context
                                .getString(R.string.msg_already_completed),Toast.LENGTH_SHORT).show()

                    }


                }else{
                    itemView.context. startActivity(Intent(itemView.context, BaseActivity::class.java)
                            .putExtra(AppConstants.CHECK,"post")
                            .putExtra("title", itemList!![adapterPosition].title)
                            .putExtra("description", itemList[adapterPosition].body)
                            .putExtra(AppConstants.USER_ID,adapterPosition.toString()))
                    ((itemView.context) as AppCompatActivity).
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
                }
            }


        }

        fun bind(postData: PostData) = with(itemView){

            if(type==1) {
                tvDes.text = postData.title + " : " + postData.body
            }else{
                tvDes.text=postData.title
            }



        }

    }
}