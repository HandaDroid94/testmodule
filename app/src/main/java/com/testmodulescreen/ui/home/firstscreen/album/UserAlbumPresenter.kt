package com.testmodulescreen.ui.home.firstscreen.album

import com.testmodulescreen.model.post.PostData
import com.testmodulescreen.network.RetrofitClient
import com.travoolala.base.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by prashant-cbl on 19/3/18.
 */
class UserAlbumPresenter : BasePresenterImpl<UserAlbumContract.View>(), UserAlbumContract.Presenter  {

    override fun apiGetUserAlbums(albumId: String) {

        getView()?.showLoader(true)
        RetrofitClient.getApi().apiGetUserAlbumImages(albumId).enqueue(object : Callback<List<PostData>> {

            override fun onResponse(call: Call<List<PostData>>?, response: Response<List<PostData>>) {
                if (response.isSuccessful) {
                    getView()?.apiSuccess(response.body())
                } else {
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)
            }

            override fun onFailure(call: Call<List<PostData>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }

        })
    }
}