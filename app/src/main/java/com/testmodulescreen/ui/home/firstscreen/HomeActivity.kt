package com.testmodulescreen.ui.home.firstscreen

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.testmodulescreen.R
import com.testmodulescreen.model.post.PostData
import com.testmodulescreen.model.user.UserData
import com.testmodulescreen.ui.customviews.LoadingDialog
import com.testmodulescreen.utils.isNetworkActive
import com.testmodulescreen.utils.showSnack
import com.testmodulescreen.utils.showSomeWWerror
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody

class HomeActivity : AppCompatActivity(), HomeContract.View {



    private val presenter= HomePresenter()
    private var loadingBox: LoadingDialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        clickEvents()
        if(this.isNetworkActive()){
            presenter.apiGetUsers()
        }
        else{
            rvUsers?.showSnack(R.string.network_error)
        }
    }

    private fun clickEvents() {


    }

    private fun init() {
        presenter.attachView(this)
        loadingBox= LoadingDialog(this)

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }


    override fun showLoader(isLoading: Boolean) {
        loadingBox?.setLoading(isLoading)
    }

    override fun apiFaliure() {
        rvAlbums?.showSomeWWerror()

    }
    override fun apiError(errorBody: ResponseBody?) {
        //this.displayApiError(errorBody, view)
    }

    override fun apiSuccess(body: List<UserData>?) {

        rvUsers.layoutManager=LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        rvUsers.adapter=UserAdapter(this,body,object : UserAdapter.OnItemClickListener {
            override fun onItemClick(userId: Int) {

                presenter.apiGetUserPost(userId,1)
                presenter.apiGetUserAlbum(userId)
                presenter.apiGetUserTodo(userId,2)

            }

        })



    }

    override fun apiSuccessPost(body: List<PostData>?) {
        rvPosts.layoutManager=LinearLayoutManager(this)
        rvPosts.isNestedScrollingEnabled=false
        rvPosts.adapter=PostAdapter(this,body,1)

    }
    override fun apiSuccessTodo(body: List<PostData>?) {
        rvTodo.layoutManager=LinearLayoutManager(this)
        rvTodo.isNestedScrollingEnabled=false
        rvTodo.adapter=PostAdapter(this,body,2)
    }

    override fun apiSuccessAlbum(body: List<PostData>?) {
        rvAlbums.layoutManager=LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        rvAlbums.adapter=AlbumAdapter(this,body)
    }

}
