package com.testmodulescreen.ui.home.firstscreen

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.testmodulescreen.R
import com.testmodulescreen.base.BaseActivity
import com.testmodulescreen.model.post.PostData
import com.testmodulescreen.utils.AppConstants
import kotlinx.android.synthetic.main.item_album.view.*

/**
 * Created by prashant-cbl on 19/3/18.
 */
class AlbumAdapter(val context: Context?, val itemList: List<PostData>?):
        RecyclerView.Adapter<AlbumAdapter.ViewHolder>()  {
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder?.bind(itemList!![position])
    }

    override fun getItemCount(): Int = itemList?.size!!


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_album,parent,false))

    inner class ViewHolder (itemView: View?) : RecyclerView.ViewHolder(itemView){

        init {
            itemView?.setOnClickListener{
                itemView.context. startActivity(Intent(itemView.context, BaseActivity::class.java)
                        .putExtra(AppConstants.CHECK,"album")
                        .putExtra("albumId", itemList!![adapterPosition].id))
                ((itemView.context) as AppCompatActivity).
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
            }


        }

        fun bind(postData: PostData) = with(itemView){

            tvTitle.text=postData.title




        }

    }
}