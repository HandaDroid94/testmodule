package com.testmodulescreen.ui.home.firstscreen

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.testmodulescreen.R
import com.testmodulescreen.model.user.UserData
import com.testmodulescreen.utils.isNetworkActive
import com.testmodulescreen.utils.showSnack
import kotlinx.android.synthetic.main.item_user.view.*

/**
 * Created by prashant-cbl on 16/3/18.
 */
class UserAdapter (val context: Context?, val itemList: List<UserData?>?,val listener: OnItemClickListener):
        RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_user,parent,false))

    override fun getItemCount(): Int = itemList?.size!!

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder?.bind()
    }

    interface OnItemClickListener {
        fun onItemClick(userId: Int)
    }

    inner class ViewHolder (itemView: View?) : RecyclerView.ViewHolder(itemView){

        init {
            itemView?.setOnClickListener{

                if(itemView.context.isNetworkActive()){
                    listener.onItemClick(itemList!![adapterPosition]?.id!!);

                }else{
                    itemView.showSnack(R.string.network_error)
                }

            }
        }




        fun bind() = with(itemView){
            Glide.with(context).load(R.drawable.placeholder)
                    .apply(RequestOptions.circleCropTransform().error(R.drawable.drawable_ring))
                    .into(ivImage)
                }

    }
}