package com.testmodulescreen.ui.home.firstscreen.post

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.testmodulescreen.R
import kotlinx.android.synthetic.main.fragment_post.*

/**
 * Created by prashant-cbl on 19/3/18.
 */
class PostFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_post,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData(view)
    }

    private fun setData(view: View) {

        tvTitle.text=arguments?.getString("title","")
        tvDes.text=arguments?.getString("description","")


    }

}