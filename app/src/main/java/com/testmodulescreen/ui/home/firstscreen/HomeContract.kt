package com.testmodulescreen.ui.home.firstscreen

import com.testmodulescreen.model.post.PostData
import com.testmodulescreen.model.user.UserData
import com.travoolala.base.BaseView

/**
 * Created by prashant-cbl on 15/1/18.
 */
interface HomeContract {

    interface View: BaseView {
        fun apiSuccess(body: List<UserData>?)
        fun apiSuccessPost(body: List<PostData>?)
        fun apiSuccessAlbum(body: List<PostData>?)
        fun apiSuccessTodo(body: List<PostData>?)
    }

    interface Presenter{
        fun apiGetUsers()
        fun apiGetUserPost(userId: Int,type:Int)
        fun apiGetUserTodo(userId: Int,type:Int)
        fun apiGetUserAlbum(userId: Int)
    }


}