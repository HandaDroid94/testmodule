package com.testmodulescreen.ui.home.firstscreen.album

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.testmodulescreen.R
import com.testmodulescreen.model.post.PostData
import kotlinx.android.synthetic.main.item_pager_album.view.*

/**
 * Created by prashant-cbl on 19/3/18.
 */
class AlbumPagerAdapter(val context: Context, val albumImages: ArrayList<PostData>) : PagerAdapter() {

    private val mLayoutInflater: LayoutInflater

    init {
        mLayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val itemView =mLayoutInflater.inflate(R.layout.item_pager_album, container, false)
        container.addView(itemView)
            Glide.with(context).load(albumImages[position].url).into(itemView.ivImage)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int =albumImages.size
}