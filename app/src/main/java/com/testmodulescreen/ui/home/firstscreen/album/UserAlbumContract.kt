package com.testmodulescreen.ui.home.firstscreen.album

import com.testmodulescreen.model.post.PostData
import com.travoolala.base.BaseView

/**
 * Created by prashant-cbl on 19/3/18.
 */
interface UserAlbumContract {

    interface View: BaseView {
        fun apiSuccess(body: List<PostData>?)
    }

    interface Presenter{
        fun apiGetUserAlbums(albumId: String)
    }


}