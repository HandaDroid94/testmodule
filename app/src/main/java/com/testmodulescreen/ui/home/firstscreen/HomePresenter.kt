package com.testmodulescreen.ui.home.firstscreen

import com.testmodulescreen.model.post.PostData
import com.testmodulescreen.model.user.UserData
import com.testmodulescreen.network.RetrofitClient
import com.travoolala.base.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by prashant-cbl on 15/1/18.
 */
class HomePresenter :BasePresenterImpl<HomeContract.View>(), HomeContract.Presenter {



    override fun apiGetUsers() {
        getView()?.showLoader(true)
        RetrofitClient.getApi().apiGetUsers().enqueue(object : Callback<List<UserData>> {

            override fun onResponse(call: Call<List<UserData>>?, response: Response<List<UserData>>) {
                if (response.isSuccessful) {
                    getView()?.apiSuccess(response.body())
                } else {
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)
            }

            override fun onFailure(call: Call<List<UserData>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }

        })

    }

    override fun apiGetUserPost(userId: Int,type:Int) {
        getView()?.showLoader(true)
        RetrofitClient.getApi().apiGetUserPost(userId).enqueue(object : Callback<List<PostData>> {

            override fun onResponse(call: Call<List<PostData>>?, response: Response<List<PostData>>) {
                if (response.isSuccessful) {
                    getView()?.apiSuccessPost(response.body())
                } else {
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)
            }

            override fun onFailure(call: Call<List<PostData>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }

        })

    }
    override fun apiGetUserTodo(userId: Int,type:Int) {
        getView()?.showLoader(true)
        RetrofitClient.getApi().apiGetUserPost(userId).enqueue(object : Callback<List<PostData>> {

            override fun onResponse(call: Call<List<PostData>>?, response: Response<List<PostData>>) {
                if (response.isSuccessful) {
                    getView()?.apiSuccessTodo(response.body())
                } else {
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)
            }

            override fun onFailure(call: Call<List<PostData>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }

        })

    }

    override fun apiGetUserAlbum(userId: Int) {
        getView()?.showLoader(true)
        RetrofitClient.getApi().apiGetUserAlbum(userId.toString()).enqueue(object : Callback<List<PostData>> {

            override fun onResponse(call: Call<List<PostData>>?, response: Response<List<PostData>>) {
                if (response.isSuccessful) {
                    getView()?.apiSuccessAlbum(response.body())
                } else {
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)
            }

            override fun onFailure(call: Call<List<PostData>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }

        })
    }


}