package com.testmodulescreen.utils

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.testmodulescreen.R
import java.text.SimpleDateFormat
import java.util.*

// for activity
fun Context.hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = activity.currentFocus
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

//for fragment
fun View.hideKeyboard(context: Context) {
    val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}


fun View.showSnack(resId: Int) {

    try {
        val snackbar = Snackbar.make(this, resId, Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        val textView = snackbarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.maxLines = 3
        /*  textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,12f)*/
        snackbar.setAction(R.string.okay, View.OnClickListener { snackbar.dismiss() })
        snackbarView.setBackgroundColor(Color.parseColor("#F3331A48"))
        snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))
        snackbar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}


fun View.showSnack( msg: String) {

    try {
        val snackbar = Snackbar.make(this, msg, Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        val textView = snackbarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.maxLines = 3
        snackbar.setAction(R.string.okay) { snackbar.dismiss() }
        snackbarView.setBackgroundColor(Color.parseColor("#F3331A48"))
        snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))
        snackbar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }



}
fun View.showSomeWWerror() {
    showSnack(R.string.sww_error)
}


fun View.showNetworkError() {
    showSnack( R.string.network_error)
}


fun Context.showMessageOKCancel( message: String) {
    android.support.v7.app.AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("OK") { dialog, which ->
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                (this as AppCompatActivity).startActivityForResult(intent, 100)
            }
            .setCancelable(false)
            .create()
            .show()
}




fun Context.isNetworkActive(): Boolean
{
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = connectivityManager.activeNetworkInfo
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting
}

fun Context.isNetworkActiveWithMessage(): Boolean
{
    val isActive = isNetworkActive()

    if (!isActive)
        Toast.makeText(this, R.string.network_error, Toast.LENGTH_SHORT).show()

    return isActive
}


fun View.compareDates(startTime: String, endTime: String, inputFormat: String): Boolean {
    val dateCompareOne = parseDate(startTime, inputFormat)
    val dateCompareTwo = parseDate(endTime, inputFormat)
    return dateCompareOne.time <= dateCompareTwo.time
}


/*
fun Context.displayApiError(errorBody: ResponseBody?, view: View?)
{
    val statusCode: Int
    val msg: String
    try {
        val jsonObject = JSONObject(errorBody?.string())
        statusCode = jsonObject.getInt("statusCode")
        msg = jsonObject.getString("message")
        if (statusCode == 401) {
            showPopUp(this, getString(R.string.bad_token_msg))
        } else
            view?.showSnack(msg)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}*/

/*
fun showPopUp(context: Context, string: String?) {
    showMessageOKCancel(string!!,context, DialogInterface.OnClickListener { dialog, which ->
        LoginManager.getInstance().logOut()
        context.clearAllNotifications()
        val reg_id = PrefsManager.get().getString(AppConstants.REG_ID, "")
        PrefsManager.get().save(AppConstants.REG_ID, reg_id)
        PrefsManager.get().removeAll()
        (context as AppCompatActivity).finishAffinity()
        context.startActivity(Intent(context, LoginSignUpActivity::class.java))
    })

}
*/


fun Context.clearAllNotifications( ) {
    val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.cancelAll()
}



/*private fun showMessageOKCancel(message: String, context: Context,
                                okListener: DialogInterface.OnClickListener) {
    AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton(R.string.ok, okListener)
            .setCancelable(false)
            .create()
            .show()
}*/



fun  getFormatFromDateUTC(date:Date,format:String):String
{
    val sdf= SimpleDateFormat(format,Locale.getDefault())
    sdf.timeZone = TimeZone.getTimeZone("UTC")
    try {
        return sdf.format(date)
    } catch (e:Exception) {
        e.printStackTrace()
        return ""
    }
}

fun getDateFromFormat(date: String,format: String):Date
{
    val sdf =SimpleDateFormat(format,Locale.getDefault())
    sdf.timeZone = TimeZone.getTimeZone("UTC")
    try {
        return sdf.parse(date)
    } catch (e: Exception) {
        e.printStackTrace()
        return Calendar.getInstance().time
    }
}

fun parseDate(date: String, inputFormat: String): Date {
    val inputParser = SimpleDateFormat(inputFormat, Locale.US)
    inputParser.timeZone = TimeZone.getTimeZone("UTC")
    try {
        return inputParser.parse(date)
    } catch (e: java.text.ParseException) {
        return Date(0)
    }

}








fun convertDpToPx(dp: Int): Int {
    return Math.round(dp * (Resources.getSystem().displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))

}

fun convertPxToDp(px: Int): Int {
    return Math.round(px / (Resources.getSystem().displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}


fun getFormatFromDate(date: Date, format: String): String {
    val sdf = SimpleDateFormat(format, Locale.US)
    try {
        return sdf.format(date)
    } catch (e: Exception) {
        e.printStackTrace()
        return ""
    }

}
