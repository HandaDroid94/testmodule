package com.testmodulescreen.utils

object AppConstants
{
 const val BASE_PATH = "https://jsonplaceholder.typicode.com/"
 //const val BASE_PATH = "http://192.168.102.100:8000/"
 val APP_TOKEN = "app_token"
 const  val CHECK = "check"
 val USER_ID = "userId"


 val DATE_FORMAT_BACKEND = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

}