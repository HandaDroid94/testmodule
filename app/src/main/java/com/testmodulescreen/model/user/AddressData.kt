package com.testmodulescreen.model.user

/**
 * Created by prashant-cbl on 16/3/18.
 */
class AddressData {

    var street: String? = null
    var suite: String? = null
    var city: String? = null
    var zipcode: String? = null
    var geo: GeoData? = null
}