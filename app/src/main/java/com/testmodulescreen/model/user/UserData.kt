package com.testmodulescreen.model.user

/**
 * Created by prashant-cbl on 16/3/18.
 */
class UserData {
    var id: Int? = null
    var name: String? = null
    var username: String? = null
    var email: String? = null
    var address: AddressData? = null
    var phone: String? = null
    var website: String? = null
    var company: Company? = null
}