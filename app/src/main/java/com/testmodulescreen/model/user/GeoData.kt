package com.testmodulescreen.model.user

/**
 * Created by prashant-cbl on 16/3/18.
 */
class GeoData {

    var lat: String? = null
    var lng: String? = null
}