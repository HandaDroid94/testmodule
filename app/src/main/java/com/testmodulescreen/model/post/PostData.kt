package com.testmodulescreen.model.post

/**
 * Created by prashant-cbl on 16/3/18.
 */
class PostData {
    var userId: Int? = null
    var id: Int? = null
    var albumId: Int? = null
    var title: String? = null
    var url: String? = null
    var thumbnailUrl: String? = null
    var completed=false
    var body: String? = null
}