package com.travoolala.model.category

data class DataItem(
	val categoryPic: CategoryPic? = null,
	val name: String? = null,
	val _id: String? = null,
	var isSelected: Boolean =false

)
