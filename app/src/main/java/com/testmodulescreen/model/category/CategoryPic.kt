package com.travoolala.model.category

data class CategoryPic(
	val thumbnail: String? = null,
	val original: String? = null
)
