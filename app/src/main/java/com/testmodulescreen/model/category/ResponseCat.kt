package com.travoolala.model.category

data class ResponseCat(
	val data: List<DataItem?>? = null,
	val message: String? = null,
	val statusCode: Int? = null
)
