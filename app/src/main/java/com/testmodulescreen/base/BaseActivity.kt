package com.testmodulescreen.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import com.testmodulescreen.R
import com.testmodulescreen.ui.home.firstscreen.album.UserAlbumFragment
import com.testmodulescreen.ui.home.firstscreen.post.PostFragment
import com.testmodulescreen.utils.AppConstants
import com.testmodulescreen.utils.hideKeyboard
import kotlinx.android.synthetic.main.activity_base.*

/**
 * Created by prashant-cbl on 19/3/18.
 */
class BaseActivity : AppCompatActivity(){

    private var bundle: Bundle?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(intent.getStringExtra("check").equals("imageViewer")){
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.activity_base)
        bundle= Bundle()
        when(intent.getStringExtra(AppConstants.CHECK)){

            "post"-> {
                bundle?.putString(AppConstants.USER_ID,intent.getStringExtra(AppConstants.USER_ID))
                bundle?.putString("title",intent.getStringExtra("title"))
                bundle?.putString("description",intent.getStringExtra("description"))
                val postFragment= PostFragment()
                postFragment.arguments=bundle
                supportFragmentManager.beginTransaction().add(R.id.frameLayout,
                        postFragment).commit()
                tvTitle.setText(R.string.posts)
            }
            "album"-> {
            bundle?.putString("albumId",intent.getIntExtra("albumId",0).toString())
            val albumFragment=UserAlbumFragment()
            albumFragment.arguments=bundle
                supportFragmentManager.beginTransaction().add(R.id.frameLayout,
                        albumFragment).commit()
                tvTitle.setText(R.string.albums)
            }

           /* "tripInvites"->
                supportFragmentManager.beginTransaction().add(R.id.frameLayout,
                        TopInvitesFragment()).commit()
            "chatScreen"->
                supportFragmentManager.beginTransaction().add(R.id.frameLayout,
                        ChatOneToOneFragment()).commit()
            "tripDetail"-> {

                val tripDetail=TripDetailFragment()
                bundle?.putString("id",intent.getStringExtra("id"))
                tripDetail.arguments=bundle
                supportFragmentManager.beginTransaction().add(R.id.frameLayout,
                        tripDetail).commit()
            } "myTrips"-> {
            supportFragmentManager.beginTransaction().add(R.id.frameLayout,
                    MyTripsFragment()).commit()
        }
            "tripListing"-> {

                val tripList= TripListingFragment()
                bundle?.putString("name",intent.getStringExtra("name"))
                tripList.arguments=bundle
                supportFragmentManager.beginTransaction().add(R.id.frameLayout,
                        tripList).commit()
            }*/
        }
    }

    override fun onBackPressed() {
        this.hideKeyboard(this)
        if (supportFragmentManager.backStackEntryCount==0 ) {
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_out_right)
        }
        else {
            supportFragmentManager.popBackStack()
        }
    }

}